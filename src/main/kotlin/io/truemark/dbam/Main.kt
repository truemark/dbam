package io.truemark.dbam

import org.apache.commons.cli.DefaultParser
import org.apache.commons.cli.HelpFormatter
import org.apache.commons.cli.Options
import org.yaml.snakeyaml.Yaml
import java.io.FileInputStream
import java.sql.Connection
import java.sql.DriverManager
import java.text.SimpleDateFormat
import java.util.*
import kotlin.system.exitProcess

fun eprintln(string: String) = System.err.println("Error: %s".format(string))

enum class DatabaseType {
  oracle, postgres
}

data class Database(
    var type: DatabaseType? = null,
    var hostname: String? = null,
    var service: String? = null,
    var port: Int? = null,
    var username: String? = null,
    var password: String? = null
)

data class Settings(
    var timezone: String? = null,
    var database: Database? = null,
    var roles: Array<String>? = null
)

data class Defaults(
    var expiration: String? = null,
    var roles: Array<String>? = null
)

data class User(
    var username: String? = null,
    var expiration: String? = null,
    var superuser: Boolean? = null,
    var roles: Array<String>? = null
)

data class Config(
    var settings: Settings? = null,
    var defaults: Defaults? = null,
    var users: Array<User>? = null
)

fun getConnectionString(config: Config): String {
  val type = config.settings?.database?.type
  val str = when(type) {
    DatabaseType.oracle -> "jdbc:oracle:thin:@//%s:%d/%s"
    DatabaseType.postgres -> "jdbc:postgresql://%s:%d/postgres"
    else -> throw IllegalArgumentException("Unsupported database type %s".format(type))
  }
  return str.format(
      config.settings?.database?.hostname,
      config.settings?.database?.port,
      config.settings?.database?.service)
}

fun getConnection(config: Config): Connection {
  val constr = getConnectionString(config)
  println("Using connection string %s".format(constr))
  val con = DriverManager.getConnection(constr,
      config.settings?.database?.username,
      config.settings?.database?.password)
  con.autoCommit = false
  return con
}

fun testConnection(config: Config, con: Connection) {
  println("Testing database connectivity")
  val type = config.settings?.database?.type
  val query = when(type) {
    DatabaseType.oracle -> "select 1 from dual"
    DatabaseType.postgres -> "select 1"
    else -> throw IllegalArgumentException("Unsupported database type %s".format(type))
  }
  with(con) {
    createStatement().use {stmt -> stmt.execute(query)}
  }
  println("Successfully connected to database")
}

fun userExists(config: Config, con: Connection, username: String): Boolean {
  val type = config.settings?.database?.type
  val query = when(type) {
    DatabaseType.oracle -> "select count(1) from dba_users where username = upper(?)"
    DatabaseType.postgres -> "select count(1) from pg_roles where rolname = lower(?)"
    else -> throw IllegalArgumentException("Unsupported database type %s".format(type))
  }
  with(con) {
    prepareStatement(query.format(username)).use { stmt ->
      stmt.setString(1, username)
      stmt.executeQuery().use { rs ->
        rs.next()
        return rs.getInt(1) == 1
      }
    }
  }
}

fun userCanLogin(config: Config, con: Connection, username: String): Boolean {
  val type = config.settings?.database?.type
  val query = when(type) {
    DatabaseType.oracle -> "select count(1) from dba_users where username = upper(?) and account_status = 'OPEN'"
    DatabaseType.postgres -> "select count(1) from pg_roles where rolname = lower(?) and rolcanlogin = true"
    else -> throw IllegalArgumentException("Unsupported database type %s".format(type))
  }
  with(con) {
    prepareStatement(query.format(username)).use { stmt ->
      stmt.setString(1, username)
      stmt.executeQuery().use { rs ->
        rs.next()
        return rs.getInt(1) == 1
      }
    }
  }
}

fun getUserRoles(config: Config, con: Connection, username: String): MutableList<String> {
  val type = config.settings?.database?.type
  val query = when(type) {
    DatabaseType.oracle -> "select granted_role from dba_role_privs where grantee = upper(?)"
    DatabaseType.postgres -> "select r.rolname from pg_auth_members m join pg_roles r on (m.roleid = r.oid) " +
        "join pg_roles u on (m.member = u.oid) where u.rolname = lower(?)"
    else -> throw IllegalArgumentException("Unsupported database type %s".format(type))
  }
  val roles = mutableListOf<String>()
  with(con) {
    prepareStatement(query.format(username)).use { stmt ->
      stmt.setString(1, username)
      stmt.executeQuery().use { rs ->
        while (rs.next()) {
          roles.add(rs.getString(1))
        }
      }
    }
  }
  return roles
}

fun createUser(config: Config, con: Connection, username: String, password: String) {
  val type = config.settings?.database?.type
  val query = when(type) {
    DatabaseType.oracle -> "create user %s identified by \"%s\""
    DatabaseType.postgres -> "create role %s with encrypted password '%s'"
    else -> throw IllegalArgumentException("Unsupported database type %s".format(type))
  }
  with(con) {
    createStatement().use { stmt ->
      stmt.execute(query.format(username, password))
      commit()
    }
  }
}

fun disableUser(config: Config, con: Connection, username: String) {
  val type = config.settings?.database?.type
  val query = when(type) {
    DatabaseType.oracle -> "alter user %s account lock"
    DatabaseType.postgres -> "alter user %s with nologin"
    else -> throw IllegalArgumentException("Unsupported database type %s".format(type))
  }
  with(con) {
    createStatement().use { stmt ->
      stmt.execute(query.format(username))
      commit()
    }
  }
}

fun enableUser(config: Config, con: Connection, username: String) {
  val type = config.settings?.database?.type
  val query = when(type) {
    DatabaseType.oracle -> "alter user %s account unlock"
    DatabaseType.postgres -> "alter user %s with login"
    else -> throw IllegalArgumentException("Unsupported database type %s".format(type))
  }
  with(con) {
    createStatement().use { stmt ->
      stmt.execute(query.format(username))
      commit()
    }
  }
}

fun revokeRole(config: Config, con: Connection, username: String, role: String) {
  val type = config.settings?.database?.type
  val query = when(type) {
    DatabaseType.oracle -> "revoke %s from %s"
    DatabaseType.postgres -> "revoke %s from %s"
    else -> throw IllegalArgumentException("Unsupported database type %s".format(type))
  }
  with (con) {
    createStatement().use { stmt ->
      stmt.execute(query.format(role, username))
      commit()
    }
  }
}

fun grantRole(config: Config, con: Connection, username: String, role: String) {
  val type = config.settings?.database?.type
  val query = when(type) {
    DatabaseType.oracle -> "grant %s to %s"
    DatabaseType.postgres -> "grant %s to %s"
    else -> throw IllegalArgumentException("Unsupported database type %s".format(type))
  }
  with (con) {
    createStatement().use { stmt ->
      stmt.execute(query.format(role, username))
      commit()
    }
  }
}

fun grantSuper(config: Config, con: Connection, username: String) {
  val type = config.settings?.database?.type
  if (type != DatabaseType.postgres) {
    return
  }
  with (con) {
    createStatement().use { stmt ->
      stmt.execute("alter user %s with superuser".format(username))
      commit()
    }
  }
}

fun revokeSuper(config: Config, con: Connection, username: String) {
  val type = config.settings?.database?.type
  if (type != DatabaseType.postgres) {
    return
  }
  with (con) {
    createStatement().use { stmt ->
      stmt.execute("alter user %s with npsuperuser".format(username))
      commit()
    }
  }
}

fun updatePassword(config: Config, con: Connection, username: String, password: String) {
  val type = config.settings?.database?.type
  val query = when(type) {
    DatabaseType.oracle -> "alter user %s identified by \"%s\""
    DatabaseType.postgres -> "alter user %s with encrypted password '%s'"
    else -> throw IllegalArgumentException("Unsupported database type %s".format(type))
  }
  with(con) {
    createStatement().use { stmt ->
      stmt.execute(query.format(username, password))
      commit()
    }
  }
}

fun isSuper(config: Config, con: Connection, username: String): Boolean {
  val type = config.settings?.database?.type
  if (type != DatabaseType.postgres) {
    return false
  }
  val query = "select count(1) from pg_roles where rolname = ? and rolsuper = true"
  with(con) {
    prepareStatement(query.format(username)).use { stmt ->
      stmt.setString(1, username)
      stmt.executeQuery().use { rs ->
        rs.next()
        return rs.getInt(1) == 1
      }
    }
  }
}

fun parseExpiration(expiration: String?, timezone: String?): Date {
  val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm")
  if (!timezone.isNullOrBlank()) {
    sdf.timeZone = TimeZone.getTimeZone(timezone)
  }
  return sdf.parse(expiration)
}

fun userExpired(user: User, timezone: String?): Boolean {
  if (user.expiration.equals("never", true)) {
    return false
  }
  return Date().after(parseExpiration(user.expiration, timezone))
}

fun generatePassword(): String {
  return "X" + UUID.randomUUID().toString()
      .replace("-", "")
      .substring(0, 29)
}

fun validatePassword(pass: String?) {
  if (pass != null) {
    if (pass.length < 8) {
      eprintln("Password must be a minimum of 8 characters")
      exitProcess(1)
    }
    if (pass.length > 30) {
      eprintln("Password mustbe less than 30 characters")
      exitProcess(1)
    }
    val pattern = "^[a-zA-Z][a-zA-Z0-9#_$]+$"
    if (!pattern.toRegex().matches(pass)) {
      eprintln("Password must match %s".format(pattern))
      exitProcess(1)
    }
  }
}

fun validateDatabase(database: Database?) {
  if (database?.type == null) {
    eprintln("Missing database type")
    exitProcess(1)
  }
  if (database.hostname.isNullOrBlank()) {
    eprintln("Missing database hostname")
    exitProcess(1)
  }
  if (database.type == DatabaseType.oracle) {
    if (database.service.isNullOrBlank()) {
      eprintln("Missing database service")
      exitProcess(1)
    }
  }
  if (database.port == null) {
    eprintln("Missing database port")
    exitProcess(1)
  }
  if (database.username.isNullOrBlank()) {
    eprintln("Missing database username")
    exitProcess(1)
  }
  if (database.password.isNullOrBlank()) {
    eprintln("Missing database password")
    exitProcess(1)
  }
}

fun validateExpiration(expiration: String) {
  if (expiration != "never") {
    try {
      parseExpiration(expiration, null)
    } catch (x: Exception) {
      eprintln("Invalid expiration %s".format(expiration))
      exitProcess(1)
    }
  }
}

fun validateDefaults(defaults: Defaults?) {
  if (defaults?.expiration.isNullOrBlank()) {
    eprintln("Default expiration is required")
    exitProcess(1)
  }
  validateExpiration(defaults?.expiration!!)
  if (defaults.roles.isNullOrEmpty()) {
    eprintln("At least one default role is required")
    exitProcess(1)
  }
}

fun validateUser(user: User) {
  if (user.username.isNullOrBlank()) {
    eprintln("Username is required")
    exitProcess(1)
  }
  if (!user.expiration.isNullOrBlank()) {
    validateExpiration(user.expiration!!)
  }
}

fun validateConfig(config: Config) {
  validateDatabase(config.settings?.database)
  validateDefaults(config.defaults)
  config.users?.forEach { user -> validateUser(user) }
}

fun main(args: Array<String>) {

  val options = Options()
  options.addOption("h", "help", false, "print help menu")
  options.addOption("p", "password", true, "database password override")
  options.addOption("d", "dryrun", false, "execute a dry run")

  val parser = DefaultParser()
  val cmd = parser.parse(options, args)

  if (cmd.hasOption("help")) {
    val formatter = HelpFormatter()
    formatter.printHelp("dbam [OPTIONS] <FILE> [USERNAME] [PASSWORD]", options)
    exitProcess(0)
  }

  var dbpass: String? = null
  if (cmd.hasOption("password")) {
    dbpass = cmd.getOptionValue("password")
  }

  val remainingArgs = cmd.argList
  if (remainingArgs.size < 1) {
    eprintln("Config file required as first parameter")
    exitProcess(1)
  }
  val filename = remainingArgs[0]

  var providedUsername: String? = null
  var providedPassword: String? = null
  val dryrun = cmd.hasOption("d")
  if (dryrun) {
    println("Running in dry run mode")
  } else {
    println("Running in apply mode")
  }

  if (remainingArgs.size > 1) {
    providedUsername = remainingArgs.get(1)
  }
  if (remainingArgs.size > 2) {
    providedPassword = remainingArgs.get(2)
  }

  // Load configuration
  println("Reading configuration from %s".format(filename))
  val config = FileInputStream(filename).use {Yaml().loadAs(it, Config::class.java)}

  if (dbpass != null) {
    println("Applying password override")
    config.settings?.database?.password = dbpass
  }

  validatePassword(providedPassword)
  validateConfig(config)

  getConnection(config).use { con ->

    testConnection(config, con)

    var found = false
    var changes = false

    // Update users
    config.users?.forEach eu@ {user ->

      if (providedUsername != null && providedUsername != user.username) {
        return@eu
      }
      found = true

      val expired = userExpired(user, config.settings?.timezone)
      val username = user.username!!
      if (expired) {
        if (userCanLogin(config, con, username)) {
          println("Disabling user %s".format(username))
          changes = true
          if (!dryrun) {
            disableUser(config, con, username)
          }
        }
      } else {
        if (!userExists(config, con, username)) {
          println("Creating user %s".format(username))
          changes = true
          if (!dryrun) {
            createUser(config, con, username, generatePassword())
          }
        }
        if (!userCanLogin(config, con, username)) {
          println("Enabling user %s".format(username))
          changes = true
          if (!dryrun) {
            enableUser(config, con, username)
          }
        }
        val currentRoles = getUserRoles(config, con, username)
        val userRoles = user.roles!!.toMutableSet()

        // revoke roles not in config
        currentRoles.forEach { currentRole ->
          if (!userRoles.contains(currentRole)) {
            println("Revoking %s from %s".format(currentRole, username))
            changes = true
            if (!dryrun) {
              revokeRole(config, con, username, currentRole)
            }
          }
        }

        // grant roles not in database
        userRoles.forEach { userRole ->
          if (!currentRoles.contains(userRole)) {
            println("Granting %s to %s".format(userRole, username))
            changes = true
            if (!dryrun) {
              grantRole(config, con, username, userRole)
            }
          }
        }

        if (config.settings?.database?.type == DatabaseType.postgres) {
          val currentSuper = isSuper(config, con, username)
          if (currentSuper && (user.superuser == null || !user.superuser!!)) {
            println("Revoking superuser from %s".format(username))
            changes = true
            if (!dryrun) {
              revokeSuper(config, con, username)
            }
          }
          if (!currentSuper && user.superuser != null && user.superuser!!) {
            print("Granting super user to %s".format(username))
            changes = true
            if (!dryrun) {
              grantSuper(config, con, username)
            }
          }
        }
      }
    }

    if (!found) {
      println("WARNING: User %s is not found in configuration file".format(providedUsername))
    } else {
      // Update password
      if (providedUsername != null && providedPassword != null) {
        println("Updating password for user %s".format(providedUsername))
        changes = true
        if (!dryrun) {
          updatePassword(config, con, providedUsername, providedPassword)
        }
      }

      if (!changes) {
        println("No changes found to apply")
      }
    }
  }
}

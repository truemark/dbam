FROM adoptopenjdk/openjdk11:latest

RUN apt-get update -qq && apt-get upgrade -qq && apt-get install -qq jq \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY target/dbam-*.jar /app/dbam.jar

ENTRYPOINT ["java", "-jar", "/app/dbam.jar"]
